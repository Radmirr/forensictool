package registryresearchtool

class FileSystemSeparator {

    companion object {

        fun getSeparator(): String {
            val isWindows = System.getProperty("os.name")
                    .toLowerCase()
                    .contains("window")
            return if (isWindows) "\\" else "/"
        }
    }
}
