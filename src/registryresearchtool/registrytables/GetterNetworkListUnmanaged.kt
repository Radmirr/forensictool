package registryresearchtool.registrytables

import registryresearchtool.FileSystemSeparator
import registryresearchtool.api.registry.ForensicRegistry

class GetterNetworkListUnmanaged(
        private val forensicRegistry: ForensicRegistry
) : Getter {

    private val separator = FileSystemSeparator.getSeparator()

    override fun getName(): String = "Network${separator}NetworkListUnmanaged.csv"

    override fun getTable(): Array<Array<String?>> {
        val data = arrayOf(
                arrayOf<String?>(
                        "DnsSuffix",
                        "FirstNetwork",
                        "Description"
                )
        )
        val localMachinePrograms = forensicRegistry.getNetworkListUnmanaged()
                .map {
                    arrayOf(
                            it.dnsSuffix,
                            it.firstNetwork,
                            it.description
                    )
                }
                .toList()
        return data.plus(localMachinePrograms)
    }
}