package registryresearchtool.registrytables

import registryresearchtool.FileSystemSeparator
import registryresearchtool.api.registry.ForensicRegistry
import java.nio.file.Paths

class GetterLocalPrograms(
        private val forensicRegistry: ForensicRegistry
) : Getter {

    private val separator = FileSystemSeparator.getSeparator()

    override fun getName(): String = "Programs${separator}LocalPrograms.csv"

    override fun getTable(): Array<Array<String?>> {
        val data = arrayOf(
                arrayOf<String?>("GUID", "Display name", "Display version", "Publisher")
        )
        val localMachinePrograms = forensicRegistry.getLocalMachineProgramsList()
                ?.map { arrayOf(it?.GUID, it?.displayName, it?.displayVersion, it?.publisher) }
                ?.toList() ?: emptyList()
        Paths.get("")
        return data.plus(localMachinePrograms)
    }
}