package registryresearchtool.registrytables

import registryresearchtool.FileSystemSeparator
import registryresearchtool.api.registry.ForensicRegistry

class GetterNetworkCard(
        private val forensicRegistry: ForensicRegistry
): Getter {

    private val separator = FileSystemSeparator.getSeparator()

    override fun getName(): String = "Network${separator}NetworkCard.csv"

    override fun getTable(): Array<Array<String?>> {
        val data = arrayOf(
                arrayOf<String?>("Description", "Service name")
        )
        val networkCards = forensicRegistry.getNetworkCard()
                .map { arrayOf(it.description, it.serviceName) }
        return data.plus(networkCards)
    }
}