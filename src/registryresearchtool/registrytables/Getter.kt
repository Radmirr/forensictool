package registryresearchtool.registrytables

interface Getter {

    fun getName(): String
    fun getTable(): Array<Array<String?>>
}