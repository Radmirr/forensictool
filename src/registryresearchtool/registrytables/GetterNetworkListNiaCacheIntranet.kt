package registryresearchtool.registrytables

import registryresearchtool.FileSystemSeparator
import registryresearchtool.api.registry.ForensicRegistry

class GetterNetworkListNiaCacheIntranet(
        private val forensicRegistry: ForensicRegistry
): Getter {

    private val separator = FileSystemSeparator.getSeparator()

    override fun getName(): String = "Network${separator}NetworkListNiaCacheIntranet.csv"

    override fun getTable(): Array<Array<String?>> {
        val data = arrayOf(
                arrayOf<String?>(
                        "Name"
                )
        )
        val localMachinePrograms = forensicRegistry.getNiaCacheIntranet()
                .map {
                    arrayOf(
                            it as String?
                    )
                }
                .toList()
        return data.plus(localMachinePrograms)
    }
}