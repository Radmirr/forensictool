package registryresearchtool.registrytables

import registryresearchtool.FileSystemSeparator
import registryresearchtool.api.registry.ForensicRegistry

class GetterOSInfo(
        private val forensicRegistry: ForensicRegistry
) : Getter {

    private val separator = FileSystemSeparator.getSeparator()

    override fun getName(): String = "OS${separator}OS Info.csv"

    override fun getTable(): Array<Array<String?>> = arrayOf(
            arrayOf("Current build", forensicRegistry.getCurrentBuild()),
            arrayOf("OS name", forensicRegistry.getNameOS()),
            arrayOf("Product ID", forensicRegistry.getProductId()),
            arrayOf("Release ID", forensicRegistry.getReleaseId()),
            arrayOf("Current time zone name", forensicRegistry.getCurrentTimeZoneInformationName()),
            arrayOf("Last used username", forensicRegistry.getLastUsedUsername())
    )
}