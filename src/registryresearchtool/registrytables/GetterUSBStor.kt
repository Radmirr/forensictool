package registryresearchtool.registrytables

import registryresearchtool.FileSystemSeparator
import registryresearchtool.api.registry.ForensicRegistry

class GetterUSBStor(
        private val forensicRegistry: ForensicRegistry
) : Getter {

    private val separator = FileSystemSeparator.getSeparator()

    override fun getName(): String = "USB${separator}USBSTOR.csv"

    override fun getTable(): Array<Array<String?>> {
        val data = arrayOf(
                arrayOf<String?>(
                        "ClassGUID",
                        "DeviceDesc",
                        "LocationINformation",
                        "Mfg",
                        "FriendlyName",
                        "Service"
                )
        )
        val localMachinePrograms = forensicRegistry.getUSBStor()
                .map { arrayOf(
                        it.classGUID,
                        it.deviceDesc,
                        it.locationInformation,
                        it.mfg,
                        it.friendlyName,
                        it.service
                ) }
                .toList()
        return data.plus(localMachinePrograms)
    }
}