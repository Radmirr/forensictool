package registryresearchtool.api.registry

enum class HKEY {
    LOCAL_MACHINE,
    CURRENT_USER
}