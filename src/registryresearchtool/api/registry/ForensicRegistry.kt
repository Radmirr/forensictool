package registryresearchtool.api.registry

import registryresearchtool.api.registry.models.NetworkCard
import registryresearchtool.api.registry.models.NetworkModel
import registryresearchtool.api.registry.models.ProgramModel
import registryresearchtool.api.registry.models.USBModel

class  ForensicRegistry(path: String) {

    private val registry: BaseRegistry = RegistryHolder().getRegisty(path)

    fun getNameOS(): String? {
        return registry.getValue(
                HKEY.LOCAL_MACHINE,
                "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion",
                "ProductName"
        )
    }

    fun getCurrentBuild(): String? {
        return registry.getValue(
                HKEY.LOCAL_MACHINE,
                "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion",
                "CurrentBuild"
        )
    }

    fun getReleaseId(): String? {
        return registry.getValue(
                HKEY.LOCAL_MACHINE,
                "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion",
                "ReleaseId"
        )
    }

    fun getProductId(): String? {
        return registry.getValue(
                HKEY.LOCAL_MACHINE,
                "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion",
                "ProductId"
        )
    }

    fun getLocalMachineProgramsList(): List<ProgramModel?>? {
        return getProgramsList(HKEY.LOCAL_MACHINE)
    }

    fun getUserProgramsList(): List<ProgramModel?>? {
        return getProgramsList(HKEY.CURRENT_USER)
    }

    private fun getProgramsList(hkey: HKEY): List<ProgramModel?>? {
        return registry.getSubkeys(
                hkey,
                "Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall"
        )?.map { guid ->
            val values = registry.getValues(
                    hkey,
                    "Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\$guid"
            )
            ProgramModel(
                    guid,
                    values?.get("DisplayName") ?: "",
                    values?.get("DisplayVersion") ?: "",
                    values?.get("InstallLocation") ?: "",
                    values?.get("Publisher") ?: ""
            )
        }?.sortedBy { it.displayName }
    }

    fun getUSB(): List<USBModel> {
        return registry.getSubkeys(
                HKEY.LOCAL_MACHINE,
                "SYSTEM\\ControlSet001\\Enum\\USB"
        )?.map { guid ->
            val subKeys = registry.getSubkeys(
                    HKEY.LOCAL_MACHINE,
                    "SYSTEM\\ControlSet001\\Enum\\USB\\$guid"
            )
            subKeys?.map {
                registry.getValues(
                        HKEY.LOCAL_MACHINE,
                        "SYSTEM\\ControlSet001\\Enum\\USB\\$guid\\$it"
                )
            }
                    ?.map {
                        USBModel(
                                deviceDesc = it?.get("DeviceDesc"),
                                classGUID = it?.get("ClassGUID"),
                                friendlyName = it?.get("FriendlyName"),
                                locationInformation = it?.get("LocationInformation"),
                                mfg = it?.get("Mfg"),
                                service = it?.get("Service")
                        )
                    }
        }?.reduce { acc, list -> list?.let { acc?.plus(it) } }
                ?: emptyList()
    }

    fun getUSBStor(): List<USBModel> {
        return registry.getSubkeys(
                HKEY.LOCAL_MACHINE,
                "SYSTEM\\ControlSet001\\Enum\\USBSTOR"
        )?.map { guid ->
            val subKeys = registry.getSubkeys(
                    HKEY.LOCAL_MACHINE,
                    "SYSTEM\\ControlSet001\\Enum\\USBSTOR\\$guid"
            )
            subKeys?.map {
                registry.getValues(
                        HKEY.LOCAL_MACHINE,
                        "SYSTEM\\ControlSet001\\Enum\\USBSTOR\\$guid\\$it"
                )
            }
                    ?.map {
                        USBModel(
                                deviceDesc = it?.get("DeviceDesc"),
                                classGUID = it?.get("ClassGUID"),
                                friendlyName = it?.get("FriendlyName"),
                                locationInformation = it?.get("LocationInformation"),
                                mfg = it?.get("Mfg"),
                                service = it?.get("Service")
                        )
                    }
        }?.reduce { acc, list -> list?.let { acc?.plus(it) } }
                ?: emptyList()
    }

    fun getNetworkListUnmanaged(): List<NetworkModel> {
        return registry.getSubkeys(
                HKEY.LOCAL_MACHINE,
                "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\NetworkList\\Signatures\\Unmanaged"
        )?.map {
            val values = registry.getValues(
                    HKEY.LOCAL_MACHINE,
                    "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\NetworkList\\Signatures\\Unmanaged\\$it"
            )
            NetworkModel(
                    firstNetwork = values?.get("FirstNetwork"),
                    description = values?.get("Description"),
                    dnsSuffix = values?.get("DnsSuffix")
            )
        } ?: emptyList()
    }

    fun getNetworkListManaged(): List<NetworkModel> {
        return registry.getSubkeys(
                HKEY.LOCAL_MACHINE,
                "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\NetworkList\\Signatures\\Managed"
        )?.map {
            val values = registry.getValues(
                    HKEY.LOCAL_MACHINE,
                    "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\NetworkList\\Signatures\\Managed\\$it"
            )
            NetworkModel(
                    firstNetwork = values?.get("FirstNetwork"),
                    description = values?.get("Description"),
                    dnsSuffix = values?.get("DnsSuffix")
            )
        } ?: emptyList()
    }

    fun getNiaCacheIntranet(): List<String> {
        return registry.getSubkeys(
                HKEY.LOCAL_MACHINE,
                "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\NetworkList\\Nla\\Cache\\Intranet"
        ) ?: emptyList()
    }

    fun getNetworkCard(): List<NetworkCard> {
        return registry.getSubkeys(
                HKEY.LOCAL_MACHINE,
                "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\NetworkCards"
        )?.map {
            val values = registry.getValues(
                    HKEY.LOCAL_MACHINE,
                    "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\NetworkCards\\$it"
            )
            NetworkCard(
                    description = values?.get("Description"),
                    serviceName = values?.get("ServiceName")
            )
        } ?: emptyList()
    }

    fun getCurrentTimeZoneInformationName() : String? = registry.getValue(
            HKEY.LOCAL_MACHINE,
            "SYSTEM\\ControlSet001\\Control\\TimeZoneInformation",
            "TimeZoneKeyName"
    )

    fun getLastUsedUsername(): String? = registry.getValue(
            HKEY.LOCAL_MACHINE,
            "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon",
            "LastUsedUsername"
    )
}

