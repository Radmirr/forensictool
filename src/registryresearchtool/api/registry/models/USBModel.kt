package registryresearchtool.api.registry.models

class USBModel(
        val deviceDesc: String?,
        val classGUID: String?,
        val service: String?,
        val locationInformation: String?,
        val mfg: String?,
        val friendlyName: String?
)