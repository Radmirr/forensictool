package registryresearchtool.api.registry.models

class NetworkModel(
        val firstNetwork: String?,
        val description: String?,
        val dnsSuffix: String?
)