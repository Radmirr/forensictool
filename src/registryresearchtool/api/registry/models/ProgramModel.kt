package registryresearchtool.api.registry.models

class ProgramModel(
        val GUID: String?,
        val displayName: String?,
        val displayVersion: String?,
        val installLocation: String?,
        val publisher: String?
)