package registryresearchtool.api.registry.models

class NetworkCard(
        val serviceName: String?,
        val description: String?
)