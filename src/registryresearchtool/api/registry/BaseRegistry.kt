package registryresearchtool.api.registry

interface BaseRegistry {
    fun getValues(hkey: HKEY, key: String): Map<String, String>?
    fun getValue(hkey: HKEY, key: String, valueName: String): String?
    fun getSubkeys(hkey: HKEY, key: String): List<String>?
}