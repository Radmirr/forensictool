package registryresearchtool.api.registry

import registryresearchtool.api.registry.rejistry.BaseRejistry

class RegistryHolder {

    // for registry files
    fun getRegisty(path: String) = BaseRejistry(path)
}