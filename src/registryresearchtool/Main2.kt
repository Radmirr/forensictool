package registryresearchtool

import registryresearchtool.gui.RegistryApp
import tornadofx.launch

fun main(args: Array<String>) {
    launch<RegistryApp>()
}