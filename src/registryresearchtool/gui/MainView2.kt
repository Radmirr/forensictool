package registryresearchtool.gui

import registryresearchtool.api.registry.ForensicRegistry
import registryresearchtool.csv.CSVWriter
import registryresearchtool.registrytables.*
import javafx.scene.control.TextField
import registryresearchtool.FileSystemSeparator
import tornadofx.*
import java.io.File
import java.lang.Exception

class MainView2 : View("Registry Research Tool") {

    private lateinit var tfSource: TextField
    private lateinit var tfResult: TextField

    override val root = vbox(10) {
        minWidth = 300.0
        padding = insets(10)

        vbox {
            spacing = 4.0

            label("Корень файловой системы")
            borderpane {
                center = vbox {
                    padding = insets(0,10,0,0)
                    tfSource = textfield()
                }
                right = vbox {
                    button("...") {
                        action { selectDir(tfSource) }
                    }
                }
            }
        }

        vbox {
            spacing = 4.0

            label("Результат")
            borderpane {
                center = vbox {
                    padding = insets(0,10,0,0)
                    tfResult = textfield()
                }
                right = vbox {
                    button("...") {
                        action { selectDir(tfResult) }
                    }
                }
            }
        }

        button("Начать исследование реестра") {
            useMaxWidth = true
            action { doResearch() }
        }
    }

    private fun selectDir(tf: TextField) {
        val dir = chooseDirectory { }

        if (dir != null) {
            tf.text = dir.path
        }
    }

    private fun doResearch() {
        val src = tfSource.text
        val out = tfResult.text

        try {
            val forensicRegistry = ForensicRegistry(src)
            val csvWriter = CSVWriter()
            val data = listOf(
                    GetterLocalPrograms(forensicRegistry),
                    GetterOSInfo(forensicRegistry),
                    GetterUSB(forensicRegistry),
                    GetterUSBStor(forensicRegistry),
                    GetterNetworkListUnmanaged(forensicRegistry),
                    GetterNetworkListManaged(forensicRegistry),
                    GetterNetworkListNiaCacheIntranet(forensicRegistry),
                    GetterNetworkCard(forensicRegistry)
            )
            data.forEach {
                csvWriter.writeToCSV(File(out, it.getName()), it.getTable())
            }

            information(
                    title = "Сообщение",
                    header = "Завершено",
                    content = "Исследование реестра завершено"
            )
        } catch (e: Exception) {
            error(
                    title = "Ошибка",
                    header = "Ошибка при исследовании реестра",
                    content = e.message
            )
        }
    }
}