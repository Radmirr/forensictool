package registryresearchtool.gui

import registryresearchtool.searchRegFiles
import javafx.scene.control.RadioButton
import javafx.scene.control.TextField
import javafx.scene.control.ToggleGroup
import tornadofx.*

class MainView : View("Registry research") {
    private val rGroup = ToggleGroup()

    private lateinit var rbAuto: RadioButton
    private lateinit var rbSelf: RadioButton
    private lateinit var tfDiscPath: TextField

    override val root = vbox {
        vbox{
            label("Выбрать файлы реестра:")
            rbAuto = radiobutton("автоматически",  rGroup)
            rbSelf = radiobutton("самостоятельно", rGroup)

            label("Путь к корню")
            hbox {
                tfDiscPath = textfield {
                }
                button("...") {
                    action { selectDir()}
                }
                enableWhen(rbAuto.selectedProperty())
            }


            button("Далее") {
                action { next()}
            }
        }
    }

    init {
        rbAuto.isSelected = true
    }

    private fun selectDir() {
        val dir = chooseDirectory {  }

        if (dir != null) {
            tfDiscPath.text = dir.absolutePath
        }
    }

    private fun next() {
        if (rbAuto.isSelected) {
            if (!tfDiscPath.text.isBlank()) {
                val regFiles = searchRegFiles()
                if (regFiles.isEmpty()) {
                    error("Ошибка", "Не найдены файлы реестра")
                } else {
                    replaceWith(SecondView(regFiles), sizeToScene = true)
                }
            } else {
                error("Ошибка", "Выберите корень файловой системы")
            }
        } else {
            replaceWith(SecondView(emptyList()), sizeToScene = true)
        }
    }
}

class SecondView(private val regFiles: List<String>) : View ("Registry research") {
    override val root = vbox {
        textarea {
            text = regFiles.reduce { asc, s -> asc + "\n$s" }.toString()
        }
    }
}