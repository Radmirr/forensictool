package registryresearchtool.csv

import java.io.File
import java.io.PrintWriter
import java.text.SimpleDateFormat
import java.util.*

class CSVWriter {

    private val separator = ";"

    fun writeToCSV(outputFile: File, data: Array<Array<String?>>){
        if (!outputFile.parentFile.exists()) {
            outputFile.parentFile.mkdirs()
        }
        val fileWriter = PrintWriter(outputFile, "cp1251")
        data.forEach {
            val line = it.map { it?.replace(";", ":") ?: "" }
                    .joinToString(separator)
                    .replace("\n", "")
                    .replace("\t", "")

            fileWriter.write(line + "\n")
        }
        fileWriter.close()
    }

    fun getFilename(fileName: String) : String {
        val formater = SimpleDateFormat("dd_MM_yyyy_HH_mm_ss", Locale.getDefault())
        return "$fileName ${formater.format(Date())}.csv"
    }
}